# Importing the libraries
import tensorflow as tf
from keras.preprocessing.image import ImageDataGenerator
from tensorflow_core.python.keras.models import model_from_json

print(tf.__version__)

# Data Preprocessing
# Preprocessing the training set
train_datagen = ImageDataGenerator(rescale=1./255, shear_range=0.2, zoom_range=0.2, horizontal_flip=True)
training_set = train_datagen.flow_from_directory('/Users/prakashiyer/Projects/machinelearning/ML-A-Z/Machine Learning A-Z Template Folder/Part 8 - Deep Learning/Section 40 - Convolutional Neural Networks (CNN)/Section 40 - Convolutional Neural Networks (CNN)/cars/training/set', target_size=(64, 64), batch_size=32, class_mode='binary')

# Preprocessing the test set
test_datagen = ImageDataGenerator(rescale=1./255)
test_set = test_datagen.flow_from_directory('/Users/prakashiyer/Projects/machinelearning/ML-A-Z/Machine Learning A-Z Template Folder/Part 8 - Deep Learning/Section 40 - Convolutional Neural Networks (CNN)/Section 40 - Convolutional Neural Networks (CNN)/cars/test/set', target_size=(64, 64), batch_size=32, class_mode='binary')
# Initialising the CNN
cnn = tf.keras.models.Sequential()

# Step 1: Convolution
cnn.add(tf.keras.layers.Conv2D(filters=32, kernel_size=3, activation='relu', input_shape=[64, 64, 3]))

# Step 2: Pooling
cnn.add(tf.keras.layers.MaxPool2D(pool_size=2, strides=2))

# Adding a second convolutional layer
cnn.add(tf.keras.layers.Conv2D(filters=32, kernel_size=3, activation='relu'))
cnn.add(tf.keras.layers.MaxPool2D(pool_size=2, strides=2))

# Step 3: Flattening
cnn.add(tf.keras.layers.Flatten())

# Step 4: Full connection
cnn.add(tf.keras.layers.Dense(units=128, activation='relu'))

# Step 5: Output Layer
cnn.add(tf.keras.layers.Dense(units=1, activation='sigmoid'))


# Part 3: Training the CNN
# Compiling the CNN

cnn.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

# Training the CNN on the training set and evaluating it on the Test set
cnn.fit(x=training_set, validation_data=test_set, epochs=25)

# serialize model to json
cnn_json = cnn.to_json()
with open("car_cnn.json", "w") as json_file:
    json_file.write(cnn_json)
# serialize weights to hd5
cnn.save_weights('cars_damage.h5')
print('Saved model to disk')

# load json and create model
json_file = open('car_cnn.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
cnn2 = model_from_json(loaded_model_json)
# load weights into new model
cnn2.load_weights('cars_damage.h5')

# Part 4: Make a single prediction
import numpy as np
from keras.preprocessing import image
test_image = image.load_img('/Users/prakashiyer/Projects/machinelearning/ML-A-Z/Machine Learning A-Z Template Folder/Part 8 - Deep Learning/Section 40 - Convolutional Neural Networks (CNN)/Section 40 - Convolutional Neural Networks (CNN)/cars/predict/img/26.jpg', target_size=(64, 64))
test_image = image.img_to_array(test_image)
test_image = np.expand_dims(test_image, axis=0)
result = cnn.predict(test_image)
training_set.class_indices
if result[0][0] == 1:
    prediction = 'good'
else:
    prediction = 'damage'
print(prediction)

result2 = cnn2.predict(test_image)
training_set.class_indices
if result2[0][0] == 1:
    prediction2 = 'good'
else:
    prediction2 = 'damage'
print(prediction2)
