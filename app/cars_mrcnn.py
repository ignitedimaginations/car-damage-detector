import os
import sys
import time
import numpy as np



import zipfile
import urllib.request
import shutil

# Root directory of the project
from keras.preprocessing.image import load_img
from keras_preprocessing.image import img_to_array
import matplotlib
matplotlib.use('TkAgg')
from matplotlib import pyplot
from matplotlib.patches import Rectangle

from mrcnn.model import MaskRCNN
from mrcnn.visualize import display_instances

ROOT_DIR = os.path.abspath("../")

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library
from mrcnn.config import Config
from mrcnn import model as modellib, utils


# Path to trained weights file
COCO_MODEL_PATH = os.path.join(ROOT_DIR, "cars_damage.h5")
PHOTO_PATH = os.path.join(ROOT_DIR, "0252.JPEG")

# Directory to save logs and model checkpoints, if not provided
# through the command line argument --logs
DEFAULT_LOGS_DIR = os.path.join(ROOT_DIR, "logs")
DEFAULT_DATASET_YEAR = "2014"

# define 81 classes that the coco model knowns about
class_names = ['good', 'damage']


class TestConfig(Config):
    """Configuration for training on MS COCO.
    Derives from the base Config class and overrides values specific
    to the COCO dataset.
    """
    # Give the configuration a recognizable name
    NAME = "test"

    # We use a GPU with 12GB memory, which can fit two images.
    # Adjust down if you use a smaller GPU.
    IMAGES_PER_GPU = 1

    # Uncomment to train on 8 GPUs (default is 1)
    # GPU_COUNT = 8

    # Number of classes (including background)
    NUM_CLASSES = 1 + 80  # COCO has 80 classes


def draw_image_with_boxes(filename, boxes_list):
    # load the image
    data = pyplot.imread(filename)

    # show the image
    pyplot.imshow(data)

    # get the context for drawing boxes
    ax = pyplot.gca()

    # plot each box
    for box in boxes_list:
        # get coordinates
        y1, x1, y2, x2 = box
        # calculate the width and height of the box
        width, height = x2 - x1, y2 - y1
        # create the shape
        rect = Rectangle((x1, y1), width, height, fill=False, color='blue')
        # draw the box
        ax.add_patch(rect)

    # show the plot
    pyplot.show()


# define the model
rcnn = MaskRCNN(mode='inference', model_dir='../', config=TestConfig())

# load coco model weights
rcnn.load_weights(COCO_MODEL_PATH, by_name=True)

# load photo
img = load_img(PHOTO_PATH)
img = img_to_array(img)

# make prediction
results = rcnn.detect([img], verbose=0)

boxes = results[0]['rois']

# visualize the results
draw_image_with_boxes(PHOTO_PATH, boxes)

# get dictionary for first prediction
r = results[0]

#display_instances(img, r['rois'], r['masks'], r['class_ids'], class_names, r['scores'])




